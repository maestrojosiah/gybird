<?php

namespace App\Repository;

use App\Entity\Car;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<Car>
 *
 * @method Car|null find($id, $lockMode = null, $lockVersion = null)
 * @method Car|null findOneBy(array $criteria, array $orderBy = null)
 * @method Car[]    findAll()
 * @method Car[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CarRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Car::class);
    }

    public function findCarsByFilters(?string $brand, ?string $model, ?int $year)
    {
        // Create a QueryBuilder instance for the Car entity
        $qb = $this->createQueryBuilder('c');
        
        // Join the associations for carMake and model
        $qb->leftJoin('c.carMake', 'cm')
           ->leftJoin('c.carModel', 'm');
        
        // Check if 'brand' is not null or empty
        if (!empty($brand)) {
            $qb->andWhere('cm.name = :brand') // Assuming 'name' is the property in the carMake entity
               ->setParameter('brand', $brand);
        }
        
        // Check if 'model' is not null or empty
        if (!empty($model)) {
            $qb->andWhere('m.name = :model') // Assuming 'name' is the property in the model entity
               ->setParameter('model', $model);
        }
        
        // Check if 'year' is not null
        if (!empty($year)) {
            $qb->andWhere('c.yom = :year')
               ->setParameter('year', $year);
        }
        
        // Execute and return the filtered result
        return $qb->getQuery()->getResult();
    }
            
//    /**
//     * @return Car[] Returns an array of Car objects
//     */
//    public function findByExampleField($value): array
//    {
//        return $this->createQueryBuilder('c')
//            ->andWhere('c.exampleField = :val')
//            ->setParameter('val', $value)
//            ->orderBy('c.id', 'ASC')
//            ->setMaxResults(10)
//            ->getQuery()
//            ->getResult()
//        ;
//    }

//    public function findOneBySomeField($value): ?Car
//    {
//        return $this->createQueryBuilder('c')
//            ->andWhere('c.exampleField = :val')
//            ->setParameter('val', $value)
//            ->getQuery()
//            ->getOneOrNullResult()
//        ;
//    }
}
