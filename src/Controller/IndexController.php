<?php

namespace App\Controller;

use App\Entity\Message;
use App\Form\ContactType;
use App\Repository\BlogCategoryRepository;
use App\Repository\BlogRepository;
use App\Repository\CarCategoryRepository;
use App\Repository\CarMakeRepository;
use App\Repository\CarModelRepository;
use App\Repository\CarRepository;
use App\Repository\ContentRepository;
use App\Repository\FAQRepository;
use App\Repository\ImageRepository;
use App\Repository\PageRepository;
use App\Repository\SEORepository;
use App\Repository\TeamMemberRepository;
use App\Repository\TestimonialsRepository;
use App\Repository\UserRepository;
use App\Service\Mailer;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;
use Symfony\Component\HttpFoundation\JsonResponse;

class IndexController extends AbstractController
{

    public function __construct(
        private EntityManagerInterface $em, 
        private readonly Mailer $mailer, 
        private readonly SEORepository $seoRepo,
        private readonly CarMakeRepository $carMakeRepository,
        private readonly CarModelRepository $carModelRepository,
        private readonly CarRepository $carRepository
     ){}

    #[Route('/', name: 'app_index')]
    public function index(CarRepository $carRepository, 
        BlogRepository $blogRepository, 
        FAQRepository $fAQRepository, 
        TestimonialsRepository $testimonialsRepository, 
        TeamMemberRepository $teamMemberRepository, 
        ContentRepository $contentRepository, 
        PageRepository $pageRepository, 
        ImageRepository $imageRepository,
    ): Response
    {
        $all_content = $contentRepository->findAll();
        $all_images = $imageRepository->findAll();
        $content = [];
        $images = [];
        $index = $pageRepository->findOneByUrl('home-page');
        $services = $pageRepository->findByIsService(true);
        $teamMembers = $teamMemberRepository->findAll();
        $testimonials = $testimonialsRepository->findAll();
        $blog_posts = $blogRepository->findPublished(true, 3);
        $cars = $carRepository->findBy(
            [],
            ['id' => 'desc'],
            8
        );
        $faqs = $fAQRepository->findBy(
            [],
            ['id' => 'asc'],
            3
        );

        foreach ($all_content as $item) {
            $content[$item->getDescription()] = $item;
        }
        foreach ($all_images as $item) {
            $images[$item->getSlug()] = $item;
        }

        list($brands, $models, $years) = $this->getBrandsModelsYears();
        
        return $this->render('index/index.html.twig', [
            'content' => $content,
            'page' => $index,
            'images' => $images,
            'services' => $services,
            'teamMembers' => $teamMembers,
            'testimonials' => $testimonials,
            'cars' => $cars,
            'faqs' => $faqs,
            'blog_posts' => $blog_posts,
            'brands' => $brands,
            'models' => $models,
            'years' => $years
        ]);
    }

    #[Route('/{page}', name: 'app_page')]
    public function page(Request $request, CarRepository $carRepository, TeamMemberRepository $teamMemberRepository, TestimonialsRepository $testimonialsRepository, BlogRepository $blogRepository, FAQRepository $fAQRepository, ContentRepository $contentRepository, PageRepository $pageRepository, ImageRepository $imageRepository, $page): Response
    {
        $carQueryParam = $request->query->get('cqp', 'nil');

        // Fetch the form inputs (they will be strings)
        $brand = $request->query->get('brand');
        $model = $request->query->get('model');
        $year = $request->query->get('year');

        // Cast 'year' to an integer if it's not empty, otherwise set it to null
        $year = !empty($year) ? (int) $year : null;

        // Fetch filtered cars from the repository
        $cars = $carRepository->findCarsByFilters($brand, $model, $year);


        // Other unchanged code...
        $all_content = $contentRepository->findAll();
        $content = [];
        $page = $pageRepository->findOneByUrl($page);
        $services = $pageRepository->findByIsService(true);
        $testimonials = $testimonialsRepository->findAll();
        $all_images = $imageRepository->findAll();
        $page_category = "Pages";
        $title = $page->getHeading();
        $faqs = $fAQRepository->findAll();
        $blog_posts = $blogRepository->findPublished(true, 50);
        $images = [];
        foreach ($all_images as $item) {
            $images[$item->getSlug()] = $item;
        }
        $teamMembers = $teamMemberRepository->findAll();
        $relatedCars = null;

        foreach ($all_content as $item) {
            $content[$item->getDescription()] = $item;
        }

        $seo = $this->seoRepo->findOneBy(
            [],
            ['id' => 'asc']
        );

        if ($carQueryParam !== "nil") {
            // Fetch the car by ID
            $car = $carRepository->find($carQueryParam);
        
            // Set the car name for SEO purposes
            $carName = $car->getCarMake() . " " . $car->getCarModel();
            $seo->setMetaTitle($carName . " Price In Kenya | " . $page->getMetaTitle());
            $seo->setMetaDescription("Calculate the amount of money you will pay for $carName, including deposit and monthly installments using our easy-to-use calculator.");
            $title = $carName . " - " . $title;
        
            // Find the total number of cars with the same make
            $totalRelatedCars = $carRepository->count(['carMake' => $car->getCarMake()]);

            // Calculate a random offset, ensuring there are enough cars to fetch 3
            $offset = max(0, rand(0, $totalRelatedCars - 3));

            // Fetch related cars with a randomized offset
            $relatedCars = $carRepository->findBy(
                ['carMake' => $car->getCarMake()],
                [],
                3,
                $offset
            );
        
            // If fewer than 3 related cars, fill up with random cars
            if (count($relatedCars) < 3) {
                $totalCars = $carRepository->count([]);
                $offset = max(0, rand(0, $totalCars - 3));
                $additionalCars = $carRepository->findBy([], [], 3 - count($relatedCars), $offset);
                $relatedCars = array_merge($relatedCars, $additionalCars);
            }
        } else {
            // No car is specified
            $seo->setMetaTitle($page->getMetaTitle() . " | Gybirds Motors Kenya");
            $seo->setMetaDescription($page->getMetaDescription());
            $car = null;
        
            // Get a random offset for fetching cars
            $totalCars = $carRepository->count([]);
            $offset = max(0, rand(0, $totalCars - 3));
        
            // Fetch 3 random cars starting from the random offset
            $relatedCars = $carRepository->findBy([], [], 3, $offset);
        }        
            
        list($brands, $models, $years) = $this->getBrandsModelsYears();

        return $this->render('index/page.html.twig', [
            'page_category' => $page_category,
            'content' => $content,
            'page' => $page,
            'cars' => $cars,
            'car' => $car,
            'myseo' => $seo,
            'relatedCars' => $relatedCars,
            'services' => $services,
            'images' => $images,
            'title' => $title,
            'teamMembers' => $teamMembers,
            'faqs' => $faqs,
            'testimonials' => $testimonials,
            'blog_posts' => $blog_posts,
            'brands' => $brands,
            'models' => $models,
            'years' => $years
        ]);
    }

    public function getBrandsModelsYears(){

        $brands = $this->carMakeRepository->createQueryBuilder('c')
            ->where('c.name IS NOT NULL')
            ->getQuery()
            ->getResult();
                
        $models = $this->carModelRepository->createQueryBuilder('c')
            ->select('DISTINCT c.name')
            ->where('c.name IS NOT NULL')
            ->getQuery()
            ->getResult();

        $years = $this->carRepository->createQueryBuilder('c')
            ->select('DISTINCT c.yom') // If 'carYear' is a simple field
            ->where('c.yom IS NOT NULL')
            ->getQuery()
            ->getResult();

        return [$brands, $models, $years];

    }

    #[Route('/service/{service}', name: 'app_service')]
    public function service(ContentRepository $contentRepository, PageRepository $pageRepository, $service): Response
    {
        $all_content = $contentRepository->findAll();
        $content = [];
        $service_page = $pageRepository->findOneByUrl($service);
        $services = $pageRepository->findByIsService(true);
        foreach ($all_content as $item) {
            $content[$item->getDescription()] = $item;
        }

       return $this->render('index/service.html.twig', [
            'content' => $content,
            'page' => $service_page,
            'services' => $services,
        ]);
    }

    #[Route('/post/{slug}', name: 'blog_detail')]
    public function blog(
        BlogCategoryRepository $blogCategoryRepository,
        BlogRepository $blogRepository,
        ImageRepository $imageRepository,
        ContentRepository $contentRepository,
        PageRepository $pageRepository,
        $slug
    ): Response {
        $blog_post = $blogRepository->findOneBySlug($slug);
        $title = $blog_post->getTitle();
        $page_category = "Blog";
        $all_content = $contentRepository->findAll();
        $blog_categories = $blogCategoryRepository->findAll();
        $all_images = $imageRepository->findAll();
        $blog_posts = $blogRepository->findPublished(true, 3);
    
        $images = [];
        foreach ($all_images as $item) {
            $images[$item->getSlug()] = $item;
        }
    
        $content = [];
        foreach ($all_content as $item) {
            $content[$item->getDescription()] = $item;
        }
    
        $blog_page = $pageRepository->findOneByUrl('blog-content');
        $blog_page->setMetaDescription($blog_post->getMetaDescription());
        $blog_page->setMetaTitle($blog_post->getMetaTitle());
    
        return $this->render('index/page.html.twig', [
            'page_category' => $page_category,
            'blog_post' => $blog_post,
            'blog_posts' => $blog_posts,
            'page' => $blog_page,
            'content' => $blog_post->getContent(),
            'blog_categories' => $blog_categories,
            'images' => $images,
            'title' => $title,
        ]);
    }
    
    
    #[Route('/showroom/{slug}', name: 'car_detail')]
    public function car(SEORepository $seoRepo, CarRepository $carRepository, ImageRepository $imageRepository, ContentRepository $contentRepository, PageRepository $pageRepository, $slug): Response
    {
        $car = $carRepository->findOneBySlug($slug);
        $related_cars = $carRepository->findBy(
            [],
            ['id' => 'desc'],
            4
        );
        $title = $car . " – Own Yours Today";
        $page_category = "Cars";
        $all_content = $contentRepository->findAll();
        $all_images = $imageRepository->findAll();
        $images = [];
        foreach ($all_images as $item) {
            $images[$item->getSlug()] = $item;
        }

        $content = [];
        foreach ($all_content as $item) {
            $content[$item->getDescription()] = $item;
        }
        
        $page = $pageRepository->findOneByUrl('car-page');
        $page->setMetaDescription($car->getMetaDescription());
        $page->setMetaTitle($car->getTitle());
        
        $seo = $seoRepo->findOneBy(
            [],
            ['id' => 'asc']
        );

        $keywords = $seo->getKeywords();

        $keywords .= ", " . $car->getCarMake() . " " . $car->getCarModel() . ", " . $car . ", " .  $car->getTitle();
        $seo->setKeywords($keywords);

        return $this->render('index/page.html.twig', [
            'car' => $car,
            'page_category' => $page_category,
            'related_cars' => $related_cars,
            'page' => $page,
            'content' => $content,
            'images' => $images,
            'title' => $title,
        ]);
    }

    #[Route('/category/{category}', name: 'car_category')]
    public function category(
        SEORepository $seoRepo, 
        CarRepository $carRepository, 
        ImageRepository $imageRepository, 
        ContentRepository $contentRepository, 
        PageRepository $pageRepository, 
        $category, 
        CarCategoryRepository $carCategoryRepository, 
        CarMakeRepository $carMakeRepository
    ): Response {
        $categories = $carCategoryRepository->findAll();
        $makes = $carMakeRepository->findAll();
        // Try to find the category in CarCategory
        $carCategory = $carCategoryRepository->findOneBySlug($category);
        $cars = $carRepository->createQueryBuilder('car')
        ->innerJoin('car.carCategories', 'category')
        ->where('category = :carCategory')
        ->setParameter('carCategory', $carCategory)
        ->orderBy('car.id', 'DESC')
        ->getQuery()
        ->getResult();
    
        // If not found, try to find it in CarMake
        if (!$carCategory) {
            $carCategory = $carMakeRepository->findOneBySlug($category);
            $cars = $carRepository->findBy(
                ['carMake' => $carCategory],
                ['id' => 'desc']
            );
    
            if (!$carCategory) {
                // Return a 404 response if not found in either
                throw $this->createNotFoundException('Category not found');
            }
    
            // Process $carMake (if needed)
            // Example: $carMake-specific logic here
        }

        list($brands, $models, $years) = $this->getBrandsModelsYears();

        $title = $carCategory->getName();
        $page_category = $carCategory->getSlug();
        $all_content = $contentRepository->findAll();
        $all_images = $imageRepository->findAll();
        $images = [];
        foreach ($all_images as $item) {
            $images[$item->getSlug()] = $item;
        }

        $content = [];
        foreach ($all_content as $item) {
            $content[$item->getDescription()] = $item;
        }
        
        $page = $pageRepository->findOneByUrl('category');
        $page->setMetaDescription($carCategory->getMetaDescription());
        $page->setMetaTitle($carCategory->getMetaTitle());
        
        $seo = $seoRepo->findOneBy(
            [],
            ['id' => 'asc']
        );

        $keywords = $seo->getKeywords();

        $keywords .= ", " . $carCategory->getMetaTitle();
        $seo->setKeywords($keywords);

        return $this->render('index/page.html.twig', [
            'category' => $carCategory,
            'page_category' => $page_category,
            'page' => $page,
            'content' => $content,
            'images' => $images,
            'brands' => $brands,
            'models' => $models,
            'years' => $years,
            'title' => $title,
            'seo' => $seo,
            'cars' => $cars,
            'categories' => $categories,
            'makes' => $makes,
        ]);
    }

    public function renderFooterForm(): Response
    {
        $form = $this->createForm(ContactType::class);
        
        return $this->render('tmp/form.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    #[Route('/api/get-current-attendant', name: 'get_current_attendant')]
    public function getCurrentAttendant(): JsonResponse
    {
        // Example data (replace with DB or config fetch)
        $attendants = [
            ['name' => 'Joyce', 'number' => '254723499239', 'image' => '/site/images/team_photos/joyce.jpg'],
            ['name' => 'Kim', 'number' => '254722208151', 'image' => '/site/images/team_photos/kim-1725950708.jpg'],
            ['name' => 'Dennis', 'number' => '254711476249', 'image' => '/site/images/team_photos/dennis-1725950623.jpg']
        ];
    
        // Fetch the current rotation index
        $rotationIndex = $this->getLastRotationIndex();
    
        // Return the current attendant
        return new JsonResponse($attendants[$rotationIndex]);
    }
        
    #[Route('/api/get-next-attendant', name: 'get_next_attendant')]
    public function getNextAttendant(): JsonResponse
    {
        // Example data (replace with DB or config fetch)
        $attendants = [
            ['name' => 'Joyce', 'number' => '254723499239', 'image' => '/site/images/team_photos/joyce.jpg'],
            ['name' => 'Kim', 'number' => '254722208151', 'image' => '/site/images/team_photos/kim-1725950708.jpg'],
            ['name' => 'Dennis', 'number' => '254711476249', 'image' => '/site/images/team_photos/dennis-1725950623.jpg']
        ];

        // Fetch last rotation index (from a DB or config fetch)
        $rotationIndex = $this->getLastRotationIndex();
        $nextIndex = ($rotationIndex + 1) % count($attendants);

        // Update rotation index (save back to DB or cache)
        $this->updateRotationIndex($nextIndex);

        // Return the next attendant
        return new JsonResponse($attendants[$nextIndex]);
    }

    private function getLastRotationIndex(): int
    {
        // Fetch the last index (from file using the configured parameter)
        $rotationFilePath = $this->getParameter('rotation_file_path');
        return (int) file_get_contents($rotationFilePath) ?: 0;
    }

    private function updateRotationIndex(int $index): void
    {
        // Save the new index (to file using the configured parameter)
        $rotationFilePath = $this->getParameter('rotation_file_path');
        file_put_contents($rotationFilePath, $index);
    }
        
    #[Route('/send/message/contact/form', name: 'contact_form')]
    public function contact_form(UserRepository $userRepo, Request $request): Response
    {

        // Create an instance of the form
        $form = $this->createForm(ContactType::class);
        
        // Handle form submission
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $contact = $form->getData();

            $name = $contact['name'];
            $email = $contact['email'];
            $subject = $contact['subject'];
            $msg = $contact['message'];
            $date = new \DateTimeImmutable();

            
            $message = new Message();
            $message->setName($name);
            $message->setEmail($email);
            $message->setSubject($subject);
            $message->setMessage($msg);
            $message->setReceivedOn($date);
            $this->em->persist($message);
            $this->em->flush();

            $subject = "Message From Contact Form";
            $admins = $userRepo->findByUsertype('admin');
            $maildata = ['name' => $name, 'emailAd' => $email, 'subject' => $subject, 'message' => $msg];

            foreach ($admins as $admin) {
                $this->mailer->sendEmailMessage($maildata, $admin->getEmail(), $subject, "contactform.html.twig");
            }
            $this->mailer->sendEmailMessage($maildata, $email, "Your message - ". $subject . " - received", "contactreceived.html.twig");
    
            $this->addFlash('success','Your Message has been sent');

        } else {

            $this->addFlash('danger','Your Message has NOT been sent');

        }
   
        return $this->redirectToRoute('app_index');

    }


    #[Route('/calculate/loan', name: 'calculate_loan', methods: ['POST'])]
    public function calculateLoan(Request $request): JsonResponse
    {
        $data = json_decode($request->getContent(), true);
        $loanAmount = $data['loanAmount'];
        $loanTerm = $data['loanTerm'];
        $loanRate = $data['loanRate'];

        $monthlyRate = $loanRate / 100 / 12;
        $equalPayment = $loanAmount * $monthlyRate / (1 - (1 + $monthlyRate) ** -$loanTerm);
        
        $balance = $loanAmount;
        $result = [];

        for ($i = 1; $i <= $loanTerm; $i++) {
            $interest = $balance * $monthlyRate;
            $principal = $equalPayment - $interest;
            $balance -= $principal;
        
            $result[] = [
                'payment' => number_format(round($equalPayment, 0)),  // Commas and whole number
                'principal' => number_format(round($principal, 0)),   // Commas and whole number
                'interest' => number_format(round($interest, 0)),     // Commas and whole number
                'balance' => number_format(round($balance, 0))        // Commas and whole number
            ];
        }
        
        return new JsonResponse($result);
            
    }

}
