<?php

namespace App\Controller\Admin;

use App\Entity\CarMake;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\IdField;
use EasyCorp\Bundle\EasyAdminBundle\Field\ImageField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextareaField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextEditorField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;

class CarMakeCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return CarMake::class;
    }


    public function configureFields(string $pageName): iterable
    {
        yield TextField::new("name")->setLabel("Make Name");
        yield TextField::new("slug")->setLabel("Make Slug");
        yield TextField::new("meta_title")->setLabel("Meta Title")
            ->hideOnIndex();
        yield TextField::new("meta_description")->setLabel("Meta Description")
            ->hideOnIndex();
        yield ImageField::new("image")->setLabel("Featured Image")
            ->setUploadDir("/public/site/images/car_images")
            ->setUploadedFileNamePattern("[slug]-[timestamp].[extension]")
            ->hideOnIndex()
            ->setBasePath("/site/images/car_images");
        yield TextField::new("keywords")->setLabel("Keywords")
            ->hideOnIndex();
        yield TextareaField::new("description")->setLabel("Make Description")
            ->hideOnIndex()
            ->setFormTypeOptions([
                'attr' => ['class' => 'ckeditor'],
            ]);   
    }

    public function configureCrud(Crud $crud): Crud
    {
        return $crud
            ->overrideTemplates([
                'crud/edit' => 'admin/ckeditor_edit.html.twig',
                'crud/new' => 'admin/ckeditor_new.html.twig',
            ]);
    }


}
