<?php

namespace App\Controller\Admin;

use App\Entity\Image;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\FormField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IdField;
use EasyCorp\Bundle\EasyAdminBundle\Field\ImageField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextEditorField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use Symfony\Component\HttpFoundation\RequestStack;

class ImageCrudController extends AbstractCrudController
{
    public function __construct(private RequestStack $requestStack)
    {
        
    }

    public function createEntity(string $entityFqcn)
    {
        $image = new Image();
    
        // Prefill the slug if passed as a query parameter
        $slug = $this->getSlug();
        if ($slug) {
            $image->setSlug($slug);
        }
    
        return $image;
    }
    
    public function getSlug(){
        $request = $this->requestStack->getCurrentRequest();
        $slug = $request->query->get('slug');
        return $slug;
    }

    public static function getEntityFqcn(): string
    {
        return Image::class;
    }


    public function configureFields(string $pageName): iterable
    {
        yield FormField::addColumn(6)->setLabel("Image Information");
        yield TextField::new("title")->setLabel("Title");
        yield TextField::new("slug")->setLabel("Slug");
        yield ImageField::new("src")->setLabel("Image")
            ->setUploadDir("/public/site/web_images")
            ->setUploadedFileNamePattern("[slug]-[timestamp].[extension]")
            ->setBasePath("/site/web_images");
    }

}
