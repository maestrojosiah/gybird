<?php

namespace App\Controller\Admin;

use App\Entity\CarImages;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Field\FormField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IdField;
use EasyCorp\Bundle\EasyAdminBundle\Field\ImageField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IntegerField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextareaField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextEditorField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;

class CarImagesCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return CarImages::class;
    }

    public function createEntity(string $entityFqcn)
    {
        $entity = new CarImages();
        $entity->setUploadedAt(new \DateTimeImmutable());

        return $entity;
    }

    public function configureFields(string $pageName): iterable
    {
        yield FormField::addColumn(6)->setLabel("Car Images");
        yield ImageField::new("image_url")->setLabel("Image URL")
            ->setUploadDir("/public/site/images/car_images")
            ->setUploadedFileNamePattern("[slug]-[timestamp].[extension]")
            ->setBasePath("/site/images/car_images");
        yield TextareaField::new("image_description")->setLabel("Image Description")
            ->hideOnIndex()
            ->setFormTypeOptions([
                'attr' => ['class' => 'ckeditor'],
            ]);        
        yield IntegerField::new("img_position")->setLabel("image Position")
            ->setHelp('eg. 4');
        yield AssociationField::new("car")->setLabel("Associated Car")
            ->autocomplete()
            ->setFormTypeOptions([
                'by_reference' => false,
            ]); 

    }

    public function configureCrud(Crud $crud): Crud
    {
        return $crud
            ->overrideTemplates([
                'crud/edit' => 'admin/ckeditor_edit.html.twig',
                'crud/new' => 'admin/ckeditor_new.html.twig',
            ]);
    }


}
