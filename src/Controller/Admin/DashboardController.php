<?php

namespace App\Controller\Admin;

use App\Entity\Blog;
use App\Entity\BlogCategory;
use App\Entity\Car;
use App\Entity\CarCategory;
use App\Entity\CarImages;
use App\Entity\CarMake;
use App\Entity\CarModel;
use App\Entity\Content;
use App\Entity\DriveType;
use App\Entity\FAQ;
use App\Entity\FuelType;
use App\Entity\Image;
use App\Entity\Message;
use App\Entity\Page;
use App\Entity\Section;
use App\Entity\SEO;
use App\Entity\Setting;
use App\Entity\TeamMember;
use App\Entity\Testimonials;
use App\Entity\Transmission;
use App\Entity\User;
use EasyCorp\Bundle\EasyAdminBundle\Config\Dashboard;
use EasyCorp\Bundle\EasyAdminBundle\Config\MenuItem;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractDashboardController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use EasyCorp\Bundle\EasyAdminBundle\Config\Action;
use EasyCorp\Bundle\EasyAdminBundle\Config\Actions;
use EasyCorp\Bundle\EasyAdminBundle\Config\Assets;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Config\UserMenu;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Http\Attribute\IsGranted;
class DashboardController extends AbstractDashboardController
{

    #[Route('/admin', name: 'admin')]
    public function index(): Response
    {
        // return parent::index();
        return $this->render('admin/index.html.twig', [
            'controller_name' => 'DashboardController',
        ]);

        // Option 1. You can make your dashboard redirect to some common page of your backend
        //
        // $adminUrlGenerator = $this->container->get(AdminUrlGenerator::class);
        // return $this->redirect($adminUrlGenerator->setController(OneOfYourCrudController::class)->generateUrl());

        // Option 2. You can make your dashboard redirect to different pages depending on the user
        //
        // if ('jane' === $this->getUser()->getUsername()) {
        //     return $this->redirect('...');
        // }

        // Option 3. You can render some custom template to display a proper dashboard with widgets, etc.
        // (tip: it's easier if your template extends from @EasyAdmin/page/content.html.twig)
        //
        // return $this->render('some/path/my-dashboard.html.twig');
    }

    public function configureDashboard(): Dashboard
    {
        return Dashboard::new()
            ->setTitle('Gybird Motors')
            ->setFaviconPath('/favicon.ico');
    }

    public function configureMenuItems(): iterable
    {
        yield MenuItem::linkToDashboard('Dashboard', 'fa fa-dashboard');

        yield Menuitem::section('Blog');
        yield MenuItem::linkToCrud('Blog', 'fas fa-pen', Blog::class);
        yield MenuItem::linkToCrud('Blog Category', 'fas fa-folder', BlogCategory::class);

        yield Menuitem::section('Cars');
        yield MenuItem::linkToCrud('Cars', 'fas fa-car', Car::class);
        yield MenuItem::linkToCrud('Car Images', 'fas fa-folder', CarImages::class);
        yield MenuItem::linkToCrud('Car Makes', 'fas fa-folder', CarMake::class);
        yield MenuItem::linkToCrud('Car Models', 'fas fa-folder', CarModel::class);
        yield MenuItem::linkToCrud('Car Transmissions', 'fas fa-folder', Transmission::class);
        yield MenuItem::linkToCrud('Fuel Types', 'fas fa-folder', FuelType::class);
        yield MenuItem::linkToCrud('Drive Types', 'fas fa-folder', DriveType::class);
        yield MenuItem::linkToCrud('Car Categories', 'fas fa-folder', CarCategory::class);

        yield Menuitem::section('Web Content');
        yield MenuItem::linkToCrud('Text Content', 'fas fa-globe', Content::class);
        yield MenuItem::linkToCrud('Image Content', 'fas fa-image', Image::class);
        yield MenuItem::linkToCrud('Sections', 'fas fa-columns', Section::class);
        yield MenuItem::linkToCrud('Pages', 'fas fa-file', Page::class);

        yield Menuitem::section('Settings');
        yield MenuItem::linkToCrud('Basic Settings', 'fas fa-gear', Setting::class);
        yield MenuItem::linkToCrud('SEO Settings', 'fas fa-search', SEO::class);

        yield Menuitem::section('Users');
        yield MenuItem::linkToCrud('Team Members', 'fas fa-users', TeamMember::class);
        yield MenuItem::linkToCrud('Testimonials', 'fas fa-comment', Testimonials::class);
        yield MenuItem::linkToCrud('User', 'fas fa-user', User::class);

        yield Menuitem::section('More');
        yield MenuItem::linkToCrud('FAQ', 'fas fa-question-circle', FAQ::class);
        yield MenuItem::linkToCrud('Messages', 'fas fa-envelope', Message::class);
        yield MenuItem::linkToUrl('Homepage', 'fa fa-home', $this->generateUrl('app_index'));
        // yield MenuItem::linkToCrud('The Label', 'fas fa-list', EntityClass::class);
    }

    public function configureActions(): Actions
    {
        return parent::configureActions()
            ->add(Crud::PAGE_INDEX, Action::DETAIL) ;
    }

    public function configureUserMenu(UserInterface $user): UserMenu
    {
        if (!$user instanceof User) {
            throw new \Exception('Wrong user');
        }

        return parent::configureUserMenu($user)
            ->setAvatarUrl("/site/images/profile_pictures/".$user->getAvatarUri())
            ->setMenuItems([
                // MenuItem::linkToUrl('My Profile','fas fa-user', $this->generateUrl('app_profile_show')),
                MenuItem::linkToUrl('Logout', 'fa fa-sign-out', $this->generateUrl('app_logout')),
            ]);

    }

    // public function configureAssets(): Assets
    // {
    //     return parent::configureAssets()
    //         ->addCssFile('/site/css/test.css'); 
    // }

    public function configureCrud(): Crud
    {
        return parent::configureCrud()
            ->setDefaultSort(
                ['id' => 'DESC']
            )
            ->setPageTitle('index', '%entity_label_plural% listing')
            ->showEntityActionsInlined();
    }


}
