<?php

namespace App\Controller\Admin;

use App\Entity\Car;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Field\BooleanField;
use EasyCorp\Bundle\EasyAdminBundle\Field\FormField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IdField;
use EasyCorp\Bundle\EasyAdminBundle\Field\ImageField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IntegerField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextareaField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextEditorField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;

class CarCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return Car::class;
    }


    public function configureFields(string $pageName): iterable
    {
        yield FormField::addColumn(6)->setLabel("Car Info");
        yield AssociationField::new("carMake")->setLabel("Make");
        yield AssociationField::new("carModel")->setLabel("Model");
        yield TextField::new("title")->setLabel("Title");
        yield TextField::new("slug")->setLabel("Slug");
        yield TextField::new("meta_description")->setLabel("Meta Description")
            ->hideOnIndex();
        yield AssociationField::new("carCategories")->setLabel("Categories")
            ->setFormTypeOptions([
                'by_reference' => false,
            ])
            ->autocomplete()
            ->hideOnIndex()
            // ->setPermission('ROLE_CONTRIBUTOR');
            ;

        yield AssociationField::new("fuelType")->setLabel("Fuel Type");
        yield AssociationField::new("transmission")->setLabel("Transmission")
            ->hideOnIndex();
        yield AssociationField::new("driveType")->setLabel("Drive Type")
            ->hideOnIndex();
        yield TextField::new("price_guide")->setLabel("Price Guide")
            ->hideOnIndex();
        yield BooleanField::new("sunroof")->setLabel("Has Sunroof?")
            ->hideOnIndex();
        yield BooleanField::new("used")->setLabel("Used?");
        yield TextField::new("zero_to_hundred_time")->setLabel("Zero To Hundred (Seconds)")
            ->setHelp('eg. 9 Seconds')
            ->hideOnIndex();
        yield IntegerField::new("engine_capacity")->setLabel("Engine Capacity")
            ->hideOnIndex();
        yield IntegerField::new("horse_power")->setLabel("Horse Power")
            ->hideOnIndex();
        yield IntegerField::new("torque")->setLabel("Torque")
            ->hideOnIndex();





        yield FormField::addColumn(6)->setLabel("More Car Information");
        yield ImageField::new("image")->setLabel("Featured Image")
            ->setUploadDir("/public/site/images/car_images")
            ->setUploadedFileNamePattern("[slug]-[timestamp].[extension]")
            ->hideOnIndex()
            ->setBasePath("/site/images/car_images");
        yield ImageField::new("transparent_bg_image")->setLabel("Transparent Background Image")
            ->setUploadDir("/public/site/images/car_images")
            ->setUploadedFileNamePattern("[slug]-[timestamp].[extension]")
            ->hideOnIndex()
            ->setBasePath("/site/images/car_images");
        yield TextareaField::new("features")->setLabel("Features")
            ->hideOnIndex()
            ->setFormTypeOptions([
                'attr' => ['class' => 'ckeditor'],
            ]);        
            yield TextareaField::new("description")->setLabel("Description")
            ->hideOnIndex()
            ->setFormTypeOptions([
                'attr' => ['class' => 'ckeditor'],
            ]);
        yield TextareaField::new("video_url")->setLabel("Video Url")
            ->hideOnIndex();
        yield TextField::new("dimensions")->setLabel("Dimensions")
            ->hideOnIndex();
        yield TextField::new("yom")->setLabel("Year Of Manufacture")
            ->hideOnIndex();

    }

    public function configureCrud(Crud $crud): Crud
    {
        return $crud
            ->overrideTemplates([
                'crud/edit' => 'admin/ckeditor_edit.html.twig',
                'crud/new' => 'admin/ckeditor_new.html.twig',
            ]);
    }



}
