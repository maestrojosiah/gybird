<?php

namespace App\Entity;

use App\Repository\PageRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: PageRepository::class)]
class Page
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $heading = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $meta_title = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $meta_description = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $url = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $featured_image = null;

    #[ORM\Column(type: Types::TEXT, nullable: true)]
    private ?string $page_content = null;

    #[ORM\Column(nullable: true)]
    private ?bool $content_first = null;

    #[ORM\Column(nullable: true)]
    private ?bool $is_service = null;

    #[ORM\ManyToMany(targetEntity: Section::class, inversedBy: 'pages')]
    private Collection $sections;

    public function __toString(): string
    {
        return (string) $this->heading;
    }

    public function __construct()
    {
        $this->sections = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getHeading(): ?string
    {
        return $this->heading;
    }

    public function setHeading(?string $heading): static
    {
        $this->heading = $heading;

        return $this;
    }

    public function getMetaTitle(): ?string
    {
        return $this->meta_title;
    }

    public function setMetaTitle(?string $meta_title): static
    {
        $this->meta_title = $meta_title;

        return $this;
    }

    public function getMetaDescription(): ?string
    {
        return $this->meta_description;
    }

    public function setMetaDescription(?string $meta_description): static
    {
        $this->meta_description = $meta_description;

        return $this;
    }

    public function getUrl(): ?string
    {
        return $this->url;
    }

    public function setUrl(?string $url): static
    {
        $this->url = $url;

        return $this;
    }

    public function getFeaturedImage(): ?string
    {
        return $this->featured_image;
    }

    public function setFeaturedImage(?string $featured_image): static
    {
        $this->featured_image = $featured_image;

        return $this;
    }

    public function getPageContent(): ?string
    {
        return $this->page_content;
    }

    public function setPageContent(?string $page_content): static
    {
        $this->page_content = $page_content;

        return $this;
    }

    public function isContentFirst(): ?bool
    {
        return $this->content_first;
    }

    public function setContentFirst(?bool $content_first): static
    {
        $this->content_first = $content_first;

        return $this;
    }

    public function isIsService(): ?bool
    {
        return $this->is_service;
    }

    public function setIsService(?bool $is_service): static
    {
        $this->is_service = $is_service;

        return $this;
    }

    /**
     * @return Collection<int, Section>
     */
    public function getSections(): Collection
    {
        return $this->sections;
    }

    public function addSection(Section $section): static
    {
        if (!$this->sections->contains($section)) {
            $this->sections->add($section);
        }

        return $this;
    }

    public function removeSection(Section $section): static
    {
        $this->sections->removeElement($section);

        return $this;
    }
}
