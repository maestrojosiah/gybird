<?php

namespace App\Entity;

use App\Repository\CarRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: CarRepository::class)]
class Car
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(nullable: true)]
    private ?int $engine_capacity = null;
    #[ORM\Column(nullable: true)]
    private ?int $horsepower = null;

    #[ORM\Column(nullable: true)]
    private ?int $torque = null;

    #[ORM\Column(type: Types::DECIMAL, precision: 10, scale: 0, nullable: true)]
    private ?string $price_guide = null;

    #[ORM\Column(nullable: true)]
    private ?bool $sunroof = null;

    #[ORM\Column(type: Types::TEXT, nullable: true)]
    private ?string $features = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $dimensions = null;

    #[ORM\Column(type: Types::DECIMAL, precision: 10, scale: 0, nullable: true)]
    private ?string $zero_to_hundred_time = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $yom = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $image = null;

    #[ORM\Column(type: Types::TEXT, nullable: true)]
    private ?string $description = null;

    #[ORM\OneToMany(targetEntity: CarImages::class, mappedBy: 'car')]
    private Collection $carImages;

    #[ORM\Column(type: Types::TEXT, nullable: true)]
    private ?string $video_url = null;

    #[ORM\Column(nullable: true)]
    private ?int $listing_position = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $title = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $transparent_bg_image = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $slug = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $meta_description = null;

    #[ORM\Column(nullable: true)]
    private ?bool $used = null;

    #[ORM\ManyToMany(targetEntity: CarCategory::class, mappedBy: 'cars')]
    private Collection $carCategories;

    #[ORM\ManyToOne(inversedBy: 'cars')]
    private ?CarMake $carMake = null;

    #[ORM\ManyToOne(inversedBy: 'cars')]
    private ?DriveType $driveType = null;

    #[ORM\ManyToOne(inversedBy: 'cars')]
    private ?FuelType $fuelType = null;

    #[ORM\ManyToOne(inversedBy: 'cars')]
    private ?Transmission $transmission = null;

    #[ORM\ManyToOne(inversedBy: 'cars')]
    private ?CarModel $carModel = null;

    public function __construct()
    {
        $this->carImages = new ArrayCollection();
        $this->carCategories = new ArrayCollection();
    }

    public function __toString()
    {
        return $this->carMake . " " . $this->carModel . " - " . $this->yom . " " . $this->engine_capacity . "cc " . $this->fuelType;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getEngineCapacity(): ?int
    {
        return $this->engine_capacity;
    }

    public function setEngineCapacity(?int $engine_capacity): static
    {
        $this->engine_capacity = $engine_capacity;

        return $this;
    }

    public function getHorsepower(): ?int
    {
        return $this->horsepower;
    }

    public function setHorsepower(?int $horsepower): static
    {
        $this->horsepower = $horsepower;

        return $this;
    }

    public function getTorque(): ?int
    {
        return $this->torque;
    }

    public function setTorque(?int $torque): static
    {
        $this->torque = $torque;

        return $this;
    }

    public function getPriceGuide(): ?string
    {
        return $this->price_guide;
    }

    public function setPriceGuide(?string $price_guide): static
    {
        $this->price_guide = $price_guide;

        return $this;
    }

    public function isSunroof(): ?bool
    {
        return $this->sunroof;
    }

    public function setSunroof(?bool $sunroof): static
    {
        $this->sunroof = $sunroof;

        return $this;
    }

    public function getFeatures(): ?string
    {
        return $this->features;
    }

    public function setFeatures(?string $features): static
    {
        $this->features = $features;

        return $this;
    }

    public function getDimensions(): ?string
    {
        return $this->dimensions;
    }

    public function setDimensions(?string $dimensions): static
    {
        $this->dimensions = $dimensions;

        return $this;
    }

    public function getZeroToHundredTime(): ?string
    {
        return $this->zero_to_hundred_time;
    }

    public function setZeroToHundredTime(?string $zero_to_hundred_time): static
    {
        $this->zero_to_hundred_time = $zero_to_hundred_time;

        return $this;
    }

    public function getYom(): ?string
    {
        return $this->yom;
    }

    public function setYom(?string $yom): static
    {
        $this->yom = $yom;

        return $this;
    }

    public function getImage(): ?string
    {
        return $this->image;
    }

    public function setImage(?string $image): static
    {
        $this->image = $image;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): static
    {
        $this->description = $description;

        return $this;
    }

    /**
     * @return Collection<int, CarImages>
     */
    public function getCarImages(): Collection
    {
        return $this->carImages;
    }

    public function addCarImage(CarImages $carImage): static
    {
        if (!$this->carImages->contains($carImage)) {
            $this->carImages->add($carImage);
            $carImage->setCar($this);
        }

        return $this;
    }

    public function removeCarImage(CarImages $carImage): static
    {
        if ($this->carImages->removeElement($carImage)) {
            // set the owning side to null (unless already changed)
            if ($carImage->getCar() === $this) {
                $carImage->setCar(null);
            }
        }

        return $this;
    }

    public function getVideoUrl(): ?string
    {
        return $this->video_url;
    }

    public function setVideoUrl(?string $video_url): static
    {
        $this->video_url = $video_url;

        return $this;
    }

    public function getListingPosition(): ?int
    {
        return $this->listing_position;
    }

    public function setListingPosition(?int $listing_position): static
    {
        $this->listing_position = $listing_position;

        return $this;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(?string $title): static
    {
        $this->title = $title;

        return $this;
    }

    public function getTransparentBgImage(): ?string
    {
        return $this->transparent_bg_image;
    }

    public function setTransparentBgImage(?string $transparent_bg_image): static
    {
        $this->transparent_bg_image = $transparent_bg_image;

        return $this;
    }

    public function getSlug(): ?string
    {
        return $this->slug;
    }

    public function setSlug(?string $slug): static
    {
        $this->slug = $slug;

        return $this;
    }

    public function getMetaDescription(): ?string
    {
        return $this->meta_description;
    }

    public function setMetaDescription(?string $meta_description): static
    {
        $this->meta_description = $meta_description;

        return $this;
    }

    public function isUsed(): ?bool
    {
        return $this->used;
    }

    public function setUsed(?bool $used): static
    {
        $this->used = $used;

        return $this;
    }

    /**
     * @return Collection<int, CarCategory>
     */
    public function getCarCategories(): Collection
    {
        return $this->carCategories;
    }

    public function addCarCategory(CarCategory $carCategory): static
    {
        if (!$this->carCategories->contains($carCategory)) {
            $this->carCategories->add($carCategory);
            $carCategory->addCar($this);
        }

        return $this;
    }

    public function removeCarCategory(CarCategory $carCategory): static
    {
        if ($this->carCategories->removeElement($carCategory)) {
            $carCategory->removeCar($this);
        }

        return $this;
    }

    public function getCarMake(): ?CarMake
    {
        return $this->carMake;
    }

    public function setCarMake(?CarMake $carMake): static
    {
        $this->carMake = $carMake;

        return $this;
    }

    public function getDriveType(): ?DriveType
    {
        return $this->driveType;
    }

    public function setDriveType(?DriveType $driveType): static
    {
        $this->driveType = $driveType;

        return $this;
    }

    public function getFuelType(): ?FuelType
    {
        return $this->fuelType;
    }

    public function setFuelType(?FuelType $fuelType): static
    {
        $this->fuelType = $fuelType;

        return $this;
    }

    public function getTransmission(): ?Transmission
    {
        return $this->transmission;
    }

    public function setTransmission(?Transmission $transmission): static
    {
        $this->transmission = $transmission;

        return $this;
    }

    public function getCarModel(): ?CarModel
    {
        return $this->carModel;
    }

    public function setCarModel(?CarModel $carModel): static
    {
        $this->carModel = $carModel;

        return $this;
    }


}
