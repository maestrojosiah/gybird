<?php

namespace App\Entity;

use App\Repository\CarModelRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: CarModelRepository::class)]
class CarModel
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 255)]
    private ?string $name = null;

    #[ORM\OneToMany(targetEntity: Car::class, mappedBy: 'carModel')]
    private Collection $cars;

    #[ORM\ManyToOne(inversedBy: 'carModels')]
    private ?CarMake $carMake = null;

    public function __construct()
    {
        $this->cars = new ArrayCollection();
    }

    public function __toString()
    {
        return $this->name;   
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): static
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return Collection<int, Car>
     */
    public function getCars(): Collection
    {
        return $this->cars;
    }

    public function addCar(Car $car): static
    {
        if (!$this->cars->contains($car)) {
            $this->cars->add($car);
            $car->setCarModel($this);
        }

        return $this;
    }

    public function removeCar(Car $car): static
    {
        if ($this->cars->removeElement($car)) {
            // set the owning side to null (unless already changed)
            if ($car->getCarModel() === $this) {
                $car->setCarModel(null);
            }
        }

        return $this;
    }

    public function getCarMake(): ?CarMake
    {
        return $this->carMake;
    }

    public function setCarMake(?CarMake $carMake): static
    {
        $this->carMake = $carMake;

        return $this;
    }

}
