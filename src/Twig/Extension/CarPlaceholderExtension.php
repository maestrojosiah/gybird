<?php 

namespace App\Twig\Extension;

use Doctrine\ORM\EntityManagerInterface;
use App\Entity\Car;
use Twig\Environment;
use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;

class CarPlaceholderExtension extends AbstractExtension
{
    public function __construct(
        private EntityManagerInterface $entityManager,
        private Environment $twig
    ) {}

    public function getFilters(): array
    {
        return [
            // Define the filter for rendering car placeholders
            new TwigFilter('render_cars', [$this, 'renderCars'], ['is_safe' => ['html']]),
        ];
    }

    public function renderCars(string $content): string
    {
        preg_match_all('/\{\{([a-z0-9\-]+)\}\}/', $content, $matches);
        $slugs = $matches[1] ?? [];
    
        if (empty($slugs)) {
            return $content;
        }
    
        $cars = $this->entityManager->getRepository(Car::class)->findBy(['slug' => $slugs]);
    
        $slugToHtml = [];
        foreach ($cars as $car) {
            $slugToHtml[$car->getSlug()] = $this->twig->render('index/_car_card.html.twig', ['car' => $car]);
        }
    
        foreach ($matches[0] as $index => $placeholder) {
            $slug = $matches[1][$index];
            $replacement = $slugToHtml[$slug] ?? ''; // Empty if no matching car found
            $content = str_replace($placeholder, $replacement, $content);
        }
    
        return $content;
    }
    
}
